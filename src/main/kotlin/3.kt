abstract class Instrument(){
    abstract fun makeSounds(repeticiones: Int)
}

class Triangle(val resonancia: Int): Instrument(){
    override fun makeSounds(repeticiones: Int) {
        for(i in 1..repeticiones){
            print("T")
            for(i in 1..resonancia){
                print("I")
            }
            print("NC")
            print(" ")
        }
        println()
    }

}
class Drump(val tipus: String): Instrument(){
    override fun makeSounds(repeticiones: Int) {
        for(i in 1..repeticiones){
            print("T")
            for(i in 1..3){
                print(tipus)
            }
            print("M")
            print(" ")
        }
        println()
    }

}
fun main(args: Array<String>) {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(1) // plays 2 times the sound
    }
}
