import java.util.Scanner

enum class ColoresArcDeSantMarti{
    ROJO, NARANJA, AMARILLO, VERDE, AÑIL, AZUL, VIOLETA
}

fun main(){
    val scanner = Scanner(System.`in`)

    val entrada = scanner.next().lowercase()
    var salida = false
    for(i in ColoresArcDeSantMarti.values()){
        if(i.toString().lowercase() == entrada){
            salida=true
        }
    }
    println(salida)
}