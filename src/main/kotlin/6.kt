import java.util.*

interface GymControlReader{
    fun nextId() : String
}
class GymControlManualReader(val scanner:Scanner = Scanner(System.`in`)): GymControlReader {
    override fun nextId() = scanner.next()
}
fun main() {
    val clientListById = mutableListOf<String>()

    for (i in 0 until 8){
        val id = GymControlManualReader().nextId()
        if(id !in clientListById){
            clientListById.add(id)
            println("$id Entrada")
        }else{
            clientListById.remove(id)
            println("$id Sortida")
        }
    }
}